var noncejs = require('./index.js');
const secret = 'MyBigDirtySecret';

var token;
if (process.argv[2]) {
	if (noncejs.check( secret, process.argv[2])) {
		console.log('Success');
	} else {
		console.log('Nonce did not work.');
	}
} else {
	console.log('Token',noncejs.generate(secret,60));
}
