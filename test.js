var noncejs = require('./index.js');
const secret = 'MyBigDirtySecret';

var token;
if (process.argv[2]) {
	noncejs.check( secret, process.argv[2] , function(err,success) {
		if (err) {
			console.log('Error',err);
		} else if (success == true) {
			console.log('Success!');
		}
	});
} else {
	noncejs.generate(secret,60, function(err,token) {
		console.log( token );
	});
}
