# NonceJS #

Nonce JS is a "Number Once" library used for single shot validation and verification processes. Nonces can also be time sensitive.

## How to use? ##


```
#!nodejs

var nonce = require('noncejs');
var token = nonce.generate('MySuperSecret',60); //Generates a token that is valid for 60 seconds
console.log('Please use ',token,' to authenticate');
```

... Somewhere else in the code...

```
#!nodejs

var nonce = require('noncejs');
nonce.check('MySuperSecret', passedNonce, function(err, success) {
   if (err) console.log('Invalid Nonce ',err);
   if (success) {
       console.log('Correct!');
   }
});

```