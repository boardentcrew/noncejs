/**
 * Very simple time senstive nonce creation and validation
 *
 * If a callback is passed then the code acts asynchronously, if not then it will act synchonously
 *
 */

var sha1 = require('sha1');
var basex = require('base-x');
var bs58 = basex('123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz');

function generateSalt(length) {
		if (length == null) length = 10;
		var chars='1234567890qwertyuopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
		var out = '';
		while(out.length < length) out = out + chars.charAt( Math.floor(Math.random()*chars.length-1) );
		return out;
}

var exports = module.exports = {
	generate:function(secret,timeoutSeconds,callback) {
		if (callback == undefined) callback = function(msg,success) { if (success == undefined) return false; return success; }; 
		if (timeoutSeconds == null) timeoutSeconds = 180;
		var salt = new Buffer(10);
		for(var i = 0;i <= 10;i++) salt.write( ("0" + Math.floor(Math.random() * 256).toString( 16 )).slice(-2), i, 1, 'hex');
		var time = Math.floor( Date.now() / 1000);
		var maxTime = time + timeoutSeconds;
		var nonce = bs58.encode( new Buffer( salt,'binary' ) ) + "0" + 
			bs58.encode( new Buffer( maxTime.toString(16), 'hex' ) ) + "0" + 
			bs58.encode( new Buffer( sha1( salt + secret + maxTime ), 'hex' ) );
		return callback(null,nonce);
	},
	check:function(secret,nonce,callback) {
		if (callback == undefined) callback = function(msg,success) { if (success == undefined) return false; return true; }; 
		var nonceParts = nonce.split("0");
		if (nonceParts.length != 3) return callback('Nonce not valid');
		if (secret.length < 2) return callback('Invalid secret');
		var salt = new Buffer( bs58.decode(nonceParts[0]), 'binary');
		var maxTime = parseInt( new Buffer( bs58.decode(nonceParts[1]),'binary').toString('hex'), 16);
		var hash = new Buffer( bs58.decode(nonceParts[2]), 'binary' ).toString('hex');
		var back = sha1( salt + secret + maxTime );
		if ( back != hash) return callback('Nonce not valid');
		if ( Math.floor( Date.now() / 1000) > maxTime) return callback('Nonce has expired');
		return callback(null,true);
	}
}
